# Devops Test Task

## Setup code repository

$ git clone https://bitbucket.org/remigiusz_boguszewicz/devops-test-task.git

## Create kubernetes cluster in GKE

Decided to use Google Kubernetes Engine because I already used that before.

Setup GKE project

    $ gcloud auth list
    $ gcloud config set account <your account>
    $ gcloud projects list
    $ gcloud config set core/project <your project>

Create kubernetes cluster

    $ gcloud container clusters create k8-test-task

Just for test

    $ kubectl run web --image=remik/remik_node:green --port=8080
    $ kubectl get pods
    NAME                  READY   STATUS    RESTARTS   AGE
    web-c99d5b64d-7q8gf   1/1     Running   0          95s

    $ kubectl port-forward web-c99d5b64d-7q8gf 8080:8080

    http://localhost:8080/
    -> nice, it works.

## Getting helm running on GKE

Following: 

- https://medium.com/google-cloud/helm-on-gke-cluster-quick-hands-on-guide-ecffad94b0

Local install

    $ sudo snap install helm --classic
    helm 2.13.1 from Snapcrafters installed

Create service account for helm

    $ cd GKE
    $ kubectl apply -f create-helm-service-account.yaml 
    serviceaccount/helm created
    clusterrolebinding.rbac.authorization.k8s.io/helm created

Initialise helm

    $ helm init --service-account helm
    $ helm version
    Client: &version.Version{SemVer:"v2.13.1"
    Server: &version.Version{SemVer:"v2.13.1"

    $ kubectl get deploy,svc tiller-deploy -n kube-system
    NAME                                  DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    deployment.extensions/tiller-deploy   1         1         1            1           2m36s

    NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)     AGE
    service/tiller-deploy   ClusterIP   10.43.253.225   <none>        44134/TCP   2m37s

Create a samplechart and install it with name helm-test

    $ helm install --name helm-test ./samplechart --set service.type=LoadBalancer

    http://34.65.208.25
    Welcome to nginx!, 
    -> nice it works

## Deploy prometheus to the cluster

Following: 

- https://blog.lwolf.org/post/going-open-source-in-monitoring-part-i-deploying-prometheus-and-grafana-to-kubernetes/

We need to create a configuration file for our chart

    $ cd devops-test-task
    $ cd prometheus
    $ helm install -f values.yaml stable/prometheus

Ok, let's see the main page

    $ export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
    $ kubectl --namespace default port-forward $POD_NAME 9090

    http://localhost:9090
    -> works

## Deploy grafana to the cluster

    $ cd devops-test-task
    $ cd grafana
    $ helm install -f values.yaml stable/grafana

Get the password stored now in secrets

    $ kubectl get secret --namespace default muddled-tuatara-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

    $ export POD_NAME=$(kubectl get pods --namespace default -l "app=grafana,release=muddled-tuatara" -o jsonpath="{.items[0].metadata.name}")
    $ kubectl --namespace default port-forward $POD_NAME 3000
    http://localhost:3000
    -> works

## Connecting Grafana and Prometheus
Login to Grafana

Data source -> Prometheus

    URL: http://jaunty-crocodile-prometheus-server.default.svc.cluster.local

Selecting Prometheus as a type for datasource will add default Prometheus dashboard to the suggested dashboards (the second tab)

Dashboards -> Import
(to import default dashboards for Prometheus)

## Deploy and configure a mysql instance in the cluster with persistent storage

Following: 

- https://medium.com/@pczarkowski/live-migrate-from-vm-based-mysql-to-kubernetes-based-mysql-48c8dab9ec3e

Just using default config:

    $ helm install --name remi stable/mysql

To get your root password run:

    $ MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace default remi-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)
    $ echo $MYSQL_ROOT_PASSWORD

## Enable logging slow logs in mysql

Following: 

- https://www.a2hosting.com/kb/developer-corner/mysql/enabling-the-slow-query-log-in-mysql
- https://dzone.com/articles/monitoring-mysql-server-metrics-using-prometheus-g

A little bit of a no-go to login directly to pod

    $ kubectl exec -it remi-mysql-7c49bf8768-k8srr -- bash
    # mysql -u root -p
    > SHOW GLOBAL VARIABLES LIKE 'slow_query_log%';
    | slow_query_log | OFF   |
    > SET GLOBAL slow_query_log = 'ON';
    | slow_query_log_file | /var/lib/mysql/remi-mysql-7c49bf8768-k8srr-slow.log |

    > SHOW GLOBAL VARIABLES LIKE 'long_query%';
    | long_query_time | 10.000000 |

    > SELECT SLEEP(20);
    # tail -f remi-mysql-7c49bf8768-k8srr-slow.log
      # Time: 2019-05-24T19:56:21.889295Z
      # User@Host: root[root] @ localhost []  Id:   636
      # Query_time: 20.000272  Lock_time: 0.000000 Rows_sent: 1  Rows_examined: 0
      SET timestamp=1558727781;
      SELECT SLEEP(20);

Nice, I can see the slow log populated when I make query run for more than a setup threshold.

## Export slow logs as metrics to prometheus server using any of suitable tools, such as telegraf

Following: 
- https://blog.nobugware.com/post/2016/telegraf_prometheus_metrics_swiss_army_knife/

First I tried to install telegraf on the mysql pod itself, which is a No-Go of course, that should be build into the docker, when pod is killed all those changes are gone.

But then I realized it is all a little weird. Why do I try to get data out of a database host, that is stored by the database in a file. It should be possible to get this data directly from database in the first place.

Let's make the slow query log accessible from database, through SQL. Then I can easily connect to database from prometheus.

Following: 

- https://tableplus.io/blog/2018/10/how-to-show-queries-log-in-mysql.html

This mysql version already has the tables implemented, we just need to enable the functionality.

    > SET global general_log = 1;
    > SET global log_output = 'table';

    > select * from mysql.slow_log;
    (as well as)
    > select * from mysql.general_log;

First creata a Data Sources in Grafana to connect to mysql instance.

Just provide: 

    Host : remi-mysql.default.svc.cluster.local:3306
    User : root
    Password : (we had that before how to get out the password from secrets)

Mysql Data Source should be established using read only account in real life, but I just use the one I already have here.

Now we can create a graph and query the tables. I would like to see number of events (rows in this table) per minute. After some play with this turns out this works pretty well:

For general log

    > select CONVERT(DATE_FORMAT(event_time,'%Y-%m-%d-%H:%i:00'),DATETIME) AS "time", 
             count(*) AS "value" 
      from mysql.general_log 
      where event_time >= DATE_SUB(NOW(),INTERVAL 6 HOUR) 
      group by CONVERT(DATE_FORMAT(event_time,'%Y-%m-%d-%H:%i:00'),DATETIME);

For slow log

    > select CONVERT(DATE_FORMAT(start_time,'%Y-%m-%d-%H:%i:00'),DATETIME) AS "time", 
             count(*) AS "value" 
      from mysql.slow_log 
      where start_time >= DATE_SUB(NOW(),INTERVAL 6 HOUR) 
      group by CONVERT(DATE_FORMAT(start_time,'%Y-%m-%d-%H:%i:00'),DATETIME);

## Setup alerting on top of exported metrics

In Grafana

    Dashboard -> (Select the one with metrics) -> Edit -> Alert 

    Condition -> (Enter Formula, Eg WHEN avg OF query (A,5m, now) IS ABOVE 5
                 Send to (select users)
                 Message: Alert: Some queries are running above the threshold time.


# Additional questions

## What problems did you encounter? How did you solve them?

Very few to be honest. First it would be impossible to finish the task without `helm`. I have never used this before and have to honestly say I am impressed. 

This work is largely based on the work of others projects I could find and I tried to be as vanilla as possible just to avoid any major troubleshooting. This turned to be the right approach. I had kubernetes cluster with grafana and prometeus running in just 2 hours. Magic thanks to GKE + helm.


## How would you setup backups for mysql in production?

Basic backup would be just to do a logical backup with mysqldump and store the files in some remote location. More sophisticated approach would be to use physical backup like Percona XtraBackup or even filesystem snapshots.

But critical part is to be able to do a point-in-time restore. Useful mostly for human errors where the requirement is similar to: "Let's restore the DB state to how it was 30min before". For that we need a last full backup plus binlogs so we can roll forward to the desired time. 

Clasic mysqldump and Percona and Severalnines has developed procedures that improve the process, I would start with going through that first.

## How would you setup failover of mysql for production in k8s? How many nodes?

That is tricky. If we assume that we should prepare ourselves to protect against the simple host failures then just running the mysql with persistent storage is enough. We will not lose any data if pod needs to be started on another host, and this will be done by kubernetes automatically.

Outside the kubernetes world we usually setup standby instances, that have completely independent storage and accept all the changes that go to binlogs and can be promoted as primary database in case of any failures with the original host. Even active-active clusters like Galera serve the DR purpose, but using them in kubernetes where we have common storage does not make much sense.

In general I would try to have as simple setup as possible. With just one instance of mysql and count on kubernetes to restart the pod on different node in case of a node failure. If we want to protect ourselves against storage failure that is a bigger fish to fry, as that would mean trying to setup active-passive setup based on storage originating from different sources? Somehow that feels weird. Not sure to be honest how I would solve this.


